#include "Header.h"

extern REG reg[MAX];

int person = 0;

int admit(void)
{
	if (person < MAX)
	{
		printf("\n등록할 이름: ");
		scanf("%s", reg[person].name);
		printf("전화번호: ");
		scanf("%s", reg[person].phone);

		printf("\n%s 정보 등록 완료!\n\n", reg[person].name);
		printf("============================================\n\n");
		person++;
		return 0;
	}
	else
	{
		printf("\n전화번호부가 가득 찼습니다!\n\n");
		printf("============================================\n\n");
	}
	return 0;
}