#include "Header.h"

REG reg[MAX];
static char * PW = "qwer1234";

void print_menu(void)
{
	printf("// 전화번호 관리 //\n\n");
	printf("1.등록   2.전체검색   3.특정한검색   4.제거   5.종료\n\n");
	printf("메뉴 선택: ");
}

int is_PassWord()
{
	static int trial = 0;
	char password[20];

	printf("비밀번호: ");
	scanf("%s", password);

	while (trial < 3)
	{
		if (!strcmp(password, PW))
		{
			trial = 0;
			return 1;
		}
		else
		{
			printf("비밀번호(%d회오류): ", ++trial);
			scanf("%s", password);
		}
	}
	printf("비밀번호(3회오류): 등록할 수 없습니다!\n\n");
	return 0;
}

int main(void)
{
	int opt;

	while (1)
	{
		print_menu();
		scanf("%d", &opt);

		switch (opt)
		{
		case 1:
		{
			if (is_PassWord())
				admit();
			else
			{
				printf("프로그램을 종료합니다!\n\n");
				printf("============================================\n\n");
				return 0;
			}
			break;
		}
		case 2:
		{
			search_all();
			break;
		}
		case 3:
		{
			search_person();
			break;
		}
		case 4:
		{
			remove();
			break;
		}
		case 5:
		{
			printf("\n프로그램을 종료합니다!\n\n");
			printf("============================================\n\n");
			return 0;
		}
		default:
		{
			printf("선택오류! 초기 화면으로 돌아갑니다.\n");
			break;
		}
		}
	}
	return 0;
}