	#include "Header.h"

extern REG reg[MAX];
extern int person;

int remove(void)
{
	int i, j;
	char name[20];

	printf("삭제할 이름: ");
	scanf("%s", name);

	for (i = 0; i < person; i++)
	{
		if (!strcmp(reg[i].name, name))
		{
			for (j = i; j < person; j++)
			{
				memcpy(&reg[j], &reg[j + 1], sizeof(reg));
			}
			printf("\n%s 정보 삭제 완료!\n\n", name);
			printf("============================================\n\n");
			person--;
			return 0;
		}
	}
}