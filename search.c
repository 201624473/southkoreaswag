#include "Header.h"

extern int person;
extern REG reg[MAX];

int search_person(void)
{
	int i;
	char search[20];

	printf("\n검색할 이름: ");
	scanf("%s", search);

	for (i = 0; i < person; i++)
	{
		if (!strcmp(reg[i].name, search))
		{
			printf("\n%s		\t%s\n\n", reg[i].name, reg[i].phone);
			printf("============================================\n\n");
			return 0;
		}
		else
			continue;
	}
	printf("\n일치하는 이름이 없습니다!\n\n");
	printf("============================================\n\n");
	return 0;
}